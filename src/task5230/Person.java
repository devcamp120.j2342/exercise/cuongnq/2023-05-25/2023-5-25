package task5230;
import java.util.Arrays;

public class Person {
    String name;
    int age;
    double weight;
    long salary;
    String[] pets;
    
    public Person(String name, int age, double weight, long salary, String[] pets) {
        this.name = name;
        this.age = age;
        this.weight = weight;
        this.salary = salary;
        this.pets = pets;
    }

    public Person(String name) {
        this.name = name;
        this.age = 18;
        this.weight = 60.5;
        this.salary = 100000000;
        this.pets = new String[] {"bigdog", "small cat ", "gold fish" , "red parrot"};

    }

    public Person() {
        this("hieuHN");
    }

    public Person(String name, int age, double weight) {
      this(name , age , weight, 20000000, new String[] {"bigdog", "small cat ", "gold fish" , "red parrot"});
    } 

    @Override
    public String toString() {
        return "Person{" + "name=" + name + ", age=" + age + ", weight=" + weight + ", salary=" + salary + ", pets=" + Arrays.toString(pets) + '}';
    }
}
