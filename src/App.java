// import model.Person;
import java.util.ArrayList;

import task5230.Person;

public class App {
    public static void main(String[] args) throws Exception {
        // System.out.println("Hello, World!");

        // Person person1 = new Person("cuong", "nguyen", "nam", "cuong", "cuong@gmail.com");
        // Person person2 = new Person();
        // System.out.println(person1.toString());
        // System.out.println(person2.toString());

        ArrayList<Person> arrayList = new ArrayList<>();

        Person person0 = new Person();
        Person person1 = new Person("Devcamp");
        Person person2 = new Person("Viet", 24 , 66.8);
        Person person3 = new Person("Nam", 30, 68.8 , 500000000, new String[] {"little camel"});

        arrayList.add(person0);
        arrayList.add(person1);
        arrayList.add(person2);
        arrayList.add(person3);

        for (Person person : arrayList) {
            System.out.println(person.toString());
        }

    }

   


}
