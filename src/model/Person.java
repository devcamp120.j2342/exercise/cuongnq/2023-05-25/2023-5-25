package model;

public class Person {
    private String firstName;
    private String lastName;
    private String Gender;
    private String contact;
    private String Email;
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public String getGender() {
        return Gender;
    }
    public void setGender(String gender) {
        Gender = gender;
    }
    public String getContact() {
        return contact;
    }
    public void setContact(String contact) {
        this.contact = contact;
    }
    public String getEmail() {
        return Email;
    }
    public void setEmail(String email) {
        Email = email;
    }
    public Person(String firstName, String lastName, String gender, String contact, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        Gender = gender;
        this.contact = contact;
        Email = email;
    }

    public Person(){};

    
}
